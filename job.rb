require 'mongoid'
require 'words_counted'
require 'stopwords'

# Class representing the datastructure in mongodb
class Job
  include Mongoid::Document

  field :name, type: String
  field :company, type: String
  field :category, type: String
  field :date, type: String
  field :location, type: String
  field :employment_type, type: String
  field :work_time, type: String
  field :full_text, type: String
  field :full_text_html, type: String
end

Mongoid.load!("mongoid.yml", :production)
# Idea : use pluck at full_text and wordscount
# freqs = WordsCounted.count(Job.all.pluck('full_text').to_s)
# puts freqs.token_frequency[60..80] # Problem "n"??

# Idee: wieviele unterschiedliche Jobs pro Kategorie? (unique)
# das meist vorkommende Wort in der Job/Spalte
# Im Volltext: was sind die meistgenannten Anforderungen/Skills

def analyze_full_texts
  filter = Stopwords::Snowball::Filter.new "de"
  filtered_text = filter.filter Job.all.pluck('full_text').to_s.split
  counter = WordsCounted.count(filtered_text.to_s)
  counter.token_frequency
end

def analyze_job_names
  
end

def analyze_job_names(category)
  
end

#puts analyze_full_texts[0..20]
