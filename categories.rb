# # All categories for jobs to crawl from the startpage of stepstone.de
# CATEGORIES = [
#   "Ingenieure-und-technische-Berufe",
#   "Naturwissenschaften-und-Forschung",
#   "IT",
#   "Aerzte",
#   "Vertrieb-und-Verkauf",
#   "Pflege-Therapie-und-Assistenz",
#   "Marketing-und-Kommunikation",
#   "Bildung-und-Soziales",
#   "Finanzen",
#   "Oeffentlicher-Dienst",
#   "Banken-Finanzdienstleister-und-Versicherungen",
#   "Recht",
#   "Einkauf-Materialwirtschaft-und-Logistik",
#   "Design-Gestaltung-und-Architektur",
#   "Personal",
#   "Handwerk-Dienstleistung-und-Fertigung",
#   "Administration-und-Sekretariat",
#   "Fuehrungskraefte"
# ]

# Retrieves Category-Links from startpage. 
def get_categories
  cats = []
  cat_doc = Nokogiri::HTML(open('https://www.stepstone.de/'))
  # there are 3 divs with a list (ul -> li's) each.
  cat_doc.xpath('/html/body/div[2]/section[1]/div/div[3]/div/div/div/nav/div').each { |div| 
    div.css('li a').each { |a| 
      # deleting href-rubbish
      cats << a.attribute('href').to_s.gsub('https://www.stepstone.de/jobs/', '').gsub('.html?clickedMainCategory=true&noAutoJaSlider=true', '')
    }
  }
  cats  
end
