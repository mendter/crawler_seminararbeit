class Crawler
  # Constant with all used selectors. Category is loaded from categories.rb
 SELECTOR = {
    :name =>                 'div.h3.job_title',
    :company =>              'div.h3.company_name',
    :location =>             '//*[@id="offerInformation"]/span[1]/span[2]',
    :employment_type =>      '//*[@id="offerInformation"]/span[3]',
    :work_time =>            '//*[@id="offerInformation"]/span[4]',
    :date =>                 '//*[@id="offerInformation"]/span[5]',
    :iframe =>               '//iframe',
    :full_text_not_iframe => '//*[@id="joboffer"]',
    :wrapper =>              '#OfferContent'
  }

  # Main method for crawling stepstone.de completely.
  def crawl
    # Categories defined in categories.rb
    get_categories.each do |cat|
      page_count = 0
      next_page = ""
      
      # If there is no "Weiter"-button, all pages of cat are crawled.
      while !next_page.nil?

        # First page to crawl is the startpage of the category. The following ones are retrieved via "next page".
        page_count == 0 ? target = "https://www.stepstone.de/jobs/" + cat + ".html" : target = "https://www.stepstone.de" + next_page

        doc = Nokogiri::HTML(open(target))
        # Looping over every job in the list
        doc.css('.job_info').each do |jobrow|
          fill_attributes(jobrow, cat)
        end

        page_count += 1
        puts "PAGE NUMBER: #{page_count}"
        next_page = doc.css('#next a').first.attribute('href')
      end
    end
  end 

  # Scraping and setting the attributes to save a new Job in database.
  def fill_attributes(jobrow, cat)

    act_job = Job.new
    # Attributes from List
    act_job.name = jobrow.css(SELECTOR[:name]).first.content         #title
    puts jobrow.css(SELECTOR[:name]).first.content #debug
    act_job.company = jobrow.css(SELECTOR[:company]).first.content   #company
    act_job.category = cat

    # Attributes from job-specific link. 
    onpage_attr = crawl_single_link("http://www.stepstone.de" + jobrow.css('div.h3.job_title a').first.attribute('href'))
    act_job.location = onpage_attr[:location]
    act_job.employment_type = onpage_attr[:employment_type]
    act_job.work_time = onpage_attr[:work_time]
    act_job.date = onpage_attr[:date]
    act_job.full_text = onpage_attr[:full_text]
    act_job.full_text_html = onpage_attr[:full_text_html]
    act_job.save
  end

  # This method gets the link of a job-entry and puts the information from there in results.
  def crawl_single_link(job_link)
    results = {}
    doc = Nokogiri::HTML(open(job_link))
    full_texts = get_full_text(doc)

    # Any of these results could be possibly empty (nil)
    results = {
      :location => not_empty(SELECTOR[:location], doc),
      :employment_type => not_empty(SELECTOR[:employment_type], doc),
      :work_time => not_empty(SELECTOR[:work_time], doc),
      :date => not_empty(SELECTOR[:date], doc),
      :full_text => full_texts[:full_text],
      :full_text_html => full_texts[:full_text_html]
    }
  end

  # Returns empty String instead of error. 
  def not_empty(path, doc)
    query = doc.xpath(path)
    query.empty? ? "" : query.first.content
  end

  # Sometimes the job-text isn't an iframe. That's when only 2 iframes are found, since these two are for ads.
  def get_full_text(doc)
    query = doc.xpath(SELECTOR[:iframe])
    texts = {}
    # the 'normal' case
    if query.count > 2
      texts = {
        :full_text => query.first.content,
        :full_text_html => query.first.to_s
      }
    elsif !doc.xpath(SELECTOR[:full_text_not_iframe]).empty?
      not_iframe = doc.xpath(SELECTOR[:full_text_not_iframe])
      texts = {
        :full_text => not_iframe.first.content,
        :full_text_html => not_iframe.to_s
      }
    else
      wrapper = doc.css(SELECTOR[:wrapper])
      texts = {
        :full_text => wrapper.first.content,
        :full_text_html => wrapper.to_s
      }
    end
  end
end