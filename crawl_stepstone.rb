require 'nokogiri'
require 'open-uri'
require 'mongoid'
require './job.rb'
require './categories.rb'
require './crawler.rb'

# Load the config for database
Mongoid.load!("mongoid.yml", :production)
# for testing
Job.delete_all

# Start to crawl
crawler = Crawler.new
crawler.crawl 