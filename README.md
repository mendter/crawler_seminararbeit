# Stepstone-Crawler #
This is Crawler for stepstone.de written in ruby. All job-data will be scraped and saved to a mongodb database.

### What do I need to run it? ###

* Ruby 2.0.0 or greater
* MongoDB Database

### Dependencies ###

* Nokogiri
* Mongoid
* Configure database in mongoid.yml

### How does it work? ###

The crawler first scrapes the category-links off the start page and then iterates over every category list.

### Who do I talk to? ###

See more: [maximilianendter.de](http://maximilianendter.de)